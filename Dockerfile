FROM node:11-slim
LABEL maintainer="Roy Meissner <meissner@informatik.uni-leipzig.de>"

ARG BUILD_ENV=LOCAL
ENV BUILD_ENV ${BUILD_ENV}

RUN mkdir /nodeApp
WORKDIR /nodeApp

# ---------------- #
#   Installation   #
# ---------------- #

COPY ./ ./
RUN if [ "$BUILD_ENV" = "CI" ] ; then npm prune --production ; else rm -R node_modules ; npm install --production ; fi
# ----------------- #
#   Configuration   #
# ----------------- #

EXPOSE 80

# ----------- #
#   Cleanup   #
# ----------- #

RUN apt-get autoremove -y && apt-get -y clean && \
		rm -rf /var/lib/apt/lists/*

# -------- #
#   Run!   #
# -------- #

CMD ["npm","start"]
