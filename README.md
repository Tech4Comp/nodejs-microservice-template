# NodeJS Microservice Template #
[![Framework](https://img.shields.io/badge/Framework-NodeJS-blue.svg)](https://nodejs.org/)
[![Webserver](https://img.shields.io/badge/Webserver-Hapi-blue.svg)](http://hapijs.com/)

This repository contains the template code for a NodeJS based Microservice. Please do **NOT** clone this repository and develop your application in it. Instead fork it and develop your application there.

### Where to start developing? ###
Have a look at the file `./server.js`, that is the main routine of this service. Follow the **require(...)** statements to get trough the entire code in the right order.

When you want to have a look at **tests**, head over to the folder `./tests`. We're using Mocha and Chai for our purposes.

Since we're developing our application with NodeJS, we're using [npm](https://docs.npmjs.com/) as a **task runner**. Have a look at the `./package.json` script section to obtain an overview of available commands. Some are:

```
# Run syntax check and lint your code
npm run lint

# Run unit tests
npm run unit:test

# Start the application
npm start
...
```

You want to **checkout this service**? Simply start the service and head over to: [http://localhost:3000/documentation](http://localhost:3000/documentation). We're using  [swagger](https://www.npmjs.com/package/hapi-swagger) to provide a API discrovery/documentation.

### Use Docker to run/test your application ###
You can use [Docker](https://www.docker.com/) to build, test and run your application locally. Simply edit the Dockerfile and run:

```
docker build -t MY_IMAGE_TAG ./
docker run -it --rm -p 8080:3000 MY_IMAGE_TAG
```

Alternatively you can use [docker-compose](https://docs.docker.com/compose/) to run your application. Simply execute:

```
docker-compose up -d
```
